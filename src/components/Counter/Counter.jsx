import React from 'react';
import PropTypes from 'prop-types';
import './Counter.css';

const Counter = ({value}) =>
  <div className="counter">
    <h2 className="counter__header">Całkowita ilość spraw</h2>
    <div className="counter__number">{value}</div>
  </div>;

Counter.propTypes = {
  value: PropTypes.number.isRequired
};

export default Counter;
