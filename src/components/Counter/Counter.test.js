/* eslint-env jest, mocha */
import React from 'react';
import renderer from 'react-test-renderer';
import Counter from './Counter.jsx';

describe('Counter', () => {
  it('renders correctly', () => {
    const component = renderer.create(
      <Counter value={88}/>
    );
    expect(component.toJSON()).toMatchSnapshot();
  });
});

