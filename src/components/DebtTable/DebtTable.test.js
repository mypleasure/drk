/* eslint-env jest, mocha */
import React from 'react';
import renderer from 'react-test-renderer';
import DebtTable from './DebtTable.jsx';
import {configure, shallow} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() });


const tableData = [
  {
    'Id': 1,
    'Number': 'DI/KOSZT/P/138483',
    'Name': 'Marcin Szymczak (Test)',
    'Value': 10000,
    'NIP': '1112223301',
    'DocumentType': 'Faktura VAT',
    'Price': 12300,
    'Address': 'ul. Paderewskiego 13 50-312 Wrocław'
  },
  {
    'Id': 2,
    'Number': 'DI/PLUS/P/4493',
    'Name': 'Kazimierz Górski (Test)',
    'Value': 4000,
    'NIP': '1112223302',
    'DocumentType': 'Inne',
    'Price': 5883,
    'Address': 'ul. Moniuszki 3/1 50-312 Wrocław'
  },
  {
    'Id': 3,
    'Number': 'DI/KOSZT/P/95757',
    'Name': 'Renata Urbańska (Test)',
    'Value': 1000,
    'NIP': '1112223303',
    'DocumentType': 'Paragon',
    'Price': 1500,
    'Address': 'ul. Jastrzębia 43 50-312 Wrocław'
  }
];

const table = shallow(<DebtTable data={tableData}/>);

describe('DebtTable', () => {
  it('renders correctly with stubbed data', () => {
    const component = renderer.create(
      <DebtTable data={tableData}/>
    );
    expect(component.toJSON()).toMatchSnapshot();
  });
});

describe('Debt row collapsed', () => {
  const row = table.find('.debt-table__row--collapsed').first();
  it('displays name, debt value and NIP', () => {
    expect(row.find('.debt-table__name').text()).toEqual('Marcin Szymczak (Test)');
    expect(row.find('.debt-table__value').text()).toEqual('10000');
    expect(row.find('.debt-table__nip').text()).toEqual('1112223301');
  });
});

describe('Toggle debt link', () => {
  const row = table.find('tbody tr').first();
  const toggleLinkExpand = row.find('.debt-table__link--expand');

  const mockedEvent = { target: { dataset: {rowId: 1}}, preventDefault: Function }

  it('Link has "Więcej" text', () => {
    expect(toggleLinkExpand).toHaveLength(1);
    expect(toggleLinkExpand.first().text()).toEqual('Więcej');
  });
  it('Text is changed to "Mniej" after click', () => {
    toggleLinkExpand.simulate('click', mockedEvent);
    expect(table.find('tbody tr').first().find('.debt-table__link--collapse').text()).toEqual('Mniej');
  });
});
