import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Alert, Table} from 'reactstrap';
import './DebtTable.css';

class DebtTable extends Component {
  constructor(props) {
    super(props);

    this.state = {
      expandedRow: null,
    };

    this.handleToggleRow = this.handleToggleRow.bind(this);
    this.getCollapsedRow = this.getCollapsedRow.bind(this);
  }


  handleToggleRow( event ) {
    event.preventDefault();
    const rowId = Number(event.target.dataset.rowId);
    if(rowId === this.state.expandedRow) {
      this.setState({expandedRow: null});
    } else {
      this.setState({expandedRow: rowId});
    }
  }

  getExpandedRow(row) {
    return <tr className="debt-table__row debt-table__row--expanded" key={row.Id}>
      <td className="debt-table__td debt-table__name">
        <div className="debt-table__inner"><div className="debt-table__caption">Dłużnik</div>{row.Name}</div>
        <div>
          <div className="debt-table__caption">Adres</div>
          {row.Address}
        </div>
      </td>
      <td className="debt-table__td debt-table__nip">
        <div className="debt-table__inner">
          <div className="debt-table__caption">NIP</div>
          {row.NIP}
        </div>
        <div className="debt-table__inner">
          <div className="debt-table__caption">Rodzaj/typ dokumentu stanowiący podstawę do wierzytelności</div>
          <div>{row.DocumentType}</div>
        </div>
      </td>
      <td className="debt-table__td debt-table__value">
        <div className="debt-table__inner">
          <div className="debt-table__caption">Kwota zadłużenia</div>
          <div>{row.Value}</div>
        </div>
        <div className="debt-table__inner">
          <div className="debt-table__caption">Cena zadłużenia</div>
          {row.Price}
        </div>
      </td>
      <td className="debt-table__td debt-table__toggle">
        <div className="debt-table__inner">
          <a href=""
            onClick={this.handleToggleRow}
            className="debt-table__link debt-table__link--collapse"
            data-row-id={row.Id}
          >
          Mniej
          </a>
        </div>
        <div className="debt-table__inner">
          <div className="debt-table__caption">Numer</div>
          {row.Number}
        </div>
      </td>
    </tr>;
  }
  getCollapsedRow(row) {
    return <tr className="debt-table__row debt-table__row--collapsed" key={row.Id}>
      <td className="debt-table__td debt-table__name">
        <div className="debt-table__inner">
          {row.Name}
        </div>
      </td>
      <td className="debt-table__td debt-table__nip">
        <div className="debt-table__inner">
          {row.NIP}
        </div>
      </td>
      <td className="debt-table__td debt-table__value">
        <div className="debt-table__inner">
          {row.Value}
        </div>
      </td>
      <td className="debt-table__td debt-table__toggle">
        <div className="debt-table__inner">
          <a href=""
            onClick={this.handleToggleRow}
            className="debt-table__link debt-table__link--expand"
            data-row-id={row.Id}
          >
          Więcej
          </a>
        </div>
      </td>
    </tr>;
  }

  render() {
    const data = this.props.data;
    if (data.length === 0) {
      return <Alert color="dark">Niestety nie posiadamy pasujących danych</Alert>;
    }
    const rows = data.map( row => {
      if(row.Id === this.state.expandedRow) {
        return this.getExpandedRow(row);
      } else {
        return this.getCollapsedRow(row);
      }
    });
    return (
      <Table responsive hover>
        <thead>
          <tr>
            <th className="debt-table__header">Dłużnik</th>
            <th className="debt-table__header">NIP</th>
            <th className="debt-table__header">Kwota zadłużenia</th>
            <th className="debt-table__header">&nbsp;</th>
          </tr>
        </thead>
        <tbody>
          {rows}
        </tbody>
      </Table>
    );
  }
}

DebtTable.propTypes = {
  data: PropTypes.array.isRequired
};

export default DebtTable;
