import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import Api from './Api';


const mock = new MockAdapter(axios);

describe('getDebtCounts', () => {
  mock.onGet('http://rekrutacja-webhosting.it.krd.pl/api/Recruitment/GetDebtsCount').reply(200, '88');

  it('returns debts count', () => {
    Api.getDebtsCount().then(response => {
      expect(response).toEqual(88);
    });
  });
});

describe('GetTopDebts', () => {

  const data = [
    {
      'Id': 1,
      'Number': 'DI/KOSZT/P/138483',
      'Name': 'Marcin Szymczak (Test)',
      'Value': 10000,
      'NIP': '1112223301',
      'DocumentType': 'Faktura VAT',
      'Price': 12300,
      'Address': 'ul. Paderewskiego 13 50-312 Wrocław'
    },
    {
      'Id': 2,
      'Number': 'DI/PLUS/P/4493',
      'Name': 'Kazimierz Górski (Test)',
      'Value': 4000,
      'NIP': '1112223302',
      'DocumentType': 'Inne',
      'Price': 5883,
      'Address': 'ul. Moniuszki 3/1 50-312 Wrocław'
    },
    {
      'Id': 3,
      'Number': 'DI/KOSZT/P/95757',
      'Name': 'Renata Urbańska (Test)',
      'Value': 1000,
      'NIP': '1112223303',
      'DocumentType': 'Paragon',
      'Price': 1500,
      'Address': 'ul. Jastrzębia 43 50-312 Wrocław'
    },
    {
      'Id': 4,
      'Number': 'DI/MON/P/4773',
      'Name': 'Mieczysław Zając (Test)',
      'Value': 1000,
      'NIP': '1112223304',
      'DocumentType': 'Inne',
      'Price': 2000,
      'Address': 'Park Rekreacyjno-Wypoczynkowy Czarny Staw Imienia Majora Pilota Czecha Nowackiego 294/41 57-340 Duszniki-Zdrój'
    },
    {
      'Id': 5,
      'Number': 'DI/KOSZT/P/39440',
      'Name': 'Marek Wiśniewski (Test)',
      'Value': 2000,
      'NIP': '1112223305',
      'DocumentType': 'Faktura VAT',
      'Price': 2500,
      'Address': 'ul. Orna 7 50-312 Wrocław'
    },
    {
      'Id': 6,
      'Number': 'DI/MON/P/1139',
      'Name': 'Piotr Szymański (Test)',
      'Value': 30000,
      'NIP': '1112223306',
      'DocumentType': 'Faktura VAT',
      'Price': 37192,
      'Address': 'ul. Ładna 3/6 50-312 Wrocław'
    },
    {
      'Id': 7,
      'Number': 'DI/KOSZT/P/3911',
      'Name': 'Sebastian Baranowski-Brzęczyszczykiewicz (Test)',
      'Value': 70000,
      'NIP': '1112223307',
      'DocumentType': 'Inne',
      'Price': 79966,
      'Address': 'ul. Piękna 1/75 50-312 Wrocław'
    },
    {
      'Id': 8,
      'Number': 'DI/PLUS/P/33758',
      'Name': 'Grażyna Mazur (Test)',
      'Value': 2000,
      'NIP': '1112223308',
      'DocumentType': 'Inne',
      'Price': 2241,
      'Address': 'ul. Kolejowa 15 50-312 Wrocław'
    },
    {
      'Id': 9,
      'Number': 'DI/KOSZT/P/23991',
      'Name': 'Małgorzata Mazurek (Test)',
      'Value': 5000,
      'NIP': '1112223309',
      'DocumentType': 'Inne',
      'Price': 5544,
      'Address': 'ul. Dąbrowskiego 33 50-312 Wrocław'
    },
    {
      'Id': 10,
      'Number': 'DI/KOSZT/P/94911',
      'Name': 'Edward Szulc (Test)',
      'Value': 10000,
      'NIP': '1112223310',
      'DocumentType': 'Faktura VAT',
      'Price': 12300,
      'Address': 'ul. POW 64 50-312 Wrocław'
    }
  ];
  mock.onGet('http://rekrutacja-webhosting.it.krd.pl/api/Recruitment/GetTopDebts').reply(200, data);

  it('returns 10 debts from database', () => {
    Api.getTopDebts().then(response => {
      expect(response).toHaveLength(10);
    });
  });

  it('returns array of objects', () => {
    Api.getTopDebts().then(response => {
      expect(response).toBeInstanceOf(Array);
      response.forEach(item => {
        expect(item).toBeInstanceOf(Object);
      });
    });
  });
});

describe('Search data type', () => {

  const TYPE_NAME = 'name';
  const TYPE_NUMBER = 'number';
  const TYPE_NIP = 'nip';

  it('return "name" type if string contains letters only or any of patterns does not match', () => {
    expect(Api.getDataType('Michal Czereśniak')).toEqual(TYPE_NAME);
    expect(Api.getDataType('t2/some5')).toEqual(TYPE_NAME);
  });
  it('return "number" type if string starts with two letters following by slash', () => {
    expect(Api.getDataType('DI/KOSZT/P/138483')).toEqual(TYPE_NUMBER);
  });
  it('return "number" type if string contains digits only', () => {
    expect(Api.getDataType('1112223301')).toEqual(TYPE_NIP);
  });
});