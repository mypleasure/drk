import axios from 'axios';

const getDebtsCount = () => {
  const endPointUrl = 'http://rekrutacja-webhosting.it.krd.pl/api/Recruitment/GetDebtsCount';
  return new Promise((resolve, reject) => {
    axios.get(endPointUrl).then(({status, data}) => {
      status === 200? resolve(data): reject( new Error('Unexpected response code. Can\'t fetch data' ));
    });
  });
};

const getTopDebts = () => {
  const endPointUrl = 'http://rekrutacja-webhosting.it.krd.pl/api/Recruitment/GetTopDebts';
  return new Promise((resolve, reject) => {
    axios.get(endPointUrl).then(({status, data}) => {
      status === 200? resolve(data): reject( new Error('Unexpected response code. Can\'t fetch data' ));
    });
  });
};


const getFilteredDebts = ( phrase = '') => {
  if( phrase.length === 0) {
    return getTopDebts();
  }

  const endPointUrl = 'http://rekrutacja-webhosting.it.krd.pl/api/Recruitment/GetFilteredDebts';
  const type = getDataType(phrase);

  return new Promise(((resolve, reject) => {
    axios.post(endPointUrl, {[type]: phrase}).then(({status, data}) => {
      status === 200? resolve(data):
        status === 405? resolve('Brak danych'): reject(new Error('Unexpected response code. Can\'t fetch data'));
    });
  }));
};

const getDataType = data => {
  if(/^[a-zA-Z]{3}/i.test(data)) {
    return 'name';
  }
  else if(/^[a-zA-Z]{2}\//.test(data)) {
    return 'number';
  }
  else if(/^\d{3,}$/.test(data)) {
    return 'nip';
  }
  else {
    return 'name';
  }
};

export default {
  getDebtsCount,
  getTopDebts,
  getFilteredDebts,
  getDataType
};
