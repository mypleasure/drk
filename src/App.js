import React, {Component} from 'react';
import './App.css';
import {Col, Container, Row} from 'reactstrap';
import Counter from './components/Counter/Counter';
import Api from './utils/Api/Api';
import DebtTable from './components/DebtTable/DebtTable';

class App extends Component {

  constructor(props) {
    super(props);

    this.state = {
      debtsCount: 0,
      debtsList: [],
      search: ''
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleSearchChange = this.handleSearchChange.bind(this);
  }

  componentDidMount() {
    Api.getDebtsCount().then(response => {
      this.setState({
        debtsCount: response
      });
    });
    Api.getTopDebts().then(response => {
      this.setState({
        debtsList: response
      });
    });
  }

  handleSubmit( event ) {
    event.preventDefault();
    Api.getFilteredDebts(this.state.search).then(response => {
      this.setState({
        debtsList: response
      });
    });
  }

  handleSearchChange( event ) {
    const value = event.target.value;
    this.setState({
      search: value
    });
  }

  render() {
    return (
      <div className="app">
        <header className="app__header">
          <Container>
            <Row className="align-items-center">
              <Col xs={12} md={9}>
                <div className="search">
                  <h2 className="search__header">Podaj numer sprawy, nazwę ub NIP dłużnika</h2>
                  <form action="" className="form-inline" onSubmit={this.handleSubmit}>
                    <input
                      type="search"
                      onChange={this.handleSearchChange}
                      value={this.state.search}
                      className="form-control search__text"
                      autoFocus
                    />
                    <button className="btn btn-danger search__submit">Szukaj</button>
                  </form>
                </div>
              </Col>
              <Col xs={12} md={3}>
                <Counter value={this.state.debtsCount} />
              </Col>
            </Row>
          </Container>
        </header>
        <main className="app__body">
          <Container>
            <DebtTable data={this.state.debtsList}/>
          </Container>
        </main>
      </div>
    );
  }
}

export default App;
