# DRK

## How to run the project locally?
1. Clone the repository
2. Ensure you have __npm__ installed globally. Follow [node installation instruction](https://docs.npmjs.com/getting-started/installing-node) if you haven't.
3. In the console cd into the cloned repository
4. Run `npm start` command

### Run tests
Replace step 4 above with `npm test` command


## Tools and libraries

* React 16
* Jest with Enzyme - for unit tests
* create-react-app (a.k.a. CRA)- zero-configuration starter fo react app
* Reactstrap - react wrapper for Bootstrap 4
* react-app-rewired - custom eslint config for CRA
* axios - for communication with backend
* BEM - naming methotology for CSS components

## Why didn't I use Less/Sass?
Because of components and BEM. I am a huge enthusiast of CSS preprocessors (I even created [simple Gulp + Sass boilerplate](https://github.com/fadehelix/yolo-gulp)) in "standard" frontend projects, but since I've been using components (like these in react) then I think that preprocessors don't give me as much as when I working on "standard" web pages where mixins, variables and rule nesting give a huge boost. 

## Tests
I like to think about tests (and TDD) like "living documentation" - I write test which describe how component/method should works and then I start to write implement solution.